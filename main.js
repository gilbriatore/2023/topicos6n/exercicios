const prompt = require("prompt-sync")();
/*
Ex 1. Escreva um algoritmo que leia um número digitado 
pelo usuário e mostre a mensagem “Número maior do
que 10! ”, caso este número seja maior, ou “Número menor 
ou igual a 10! ”, caso este número seja menor ou igual.
*/
function executarExercio01(){
    let numero = prompt("Digite um número:");
    if (numero > 10){
        console.log("Número maior do que 10!");
    } else {
        console.log("Número menor ou igual 10!");
    }
}

//executarExercio01();

/*
Ex 2. Escreva um algoritmo que leia dois 
números digitados pelo usuário e exiba 
o resultado da sua soma.
*/
const executarExercicio02 = ()=>{
    let primeiroNumero = Number(prompt("Digite o primeiro número: "));    
    let segundoNumero = prompt("Digite o segundo número: ");
    console.log(typeof primeiroNumero);
    console.log(typeof segundoNumero);
    const resultado = primeiroNumero + parseFloat(segundoNumero);
    console.log(resultado)
}

executarExercicio02();
